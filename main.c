#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

#define KILL_SWITCH "-k"
#define GREP "grep "
#define PIPE '|'
#define PS "ps -xeo pid,comm "

void runCommand(const char*);
void killProcess(int);
char* extractSwitches(const char*);

int main(int argc, char const *argv[])
{
  char c;
  int kflg, errflg, targetPid;
  char *ifile;
  char *ofile;
  extern char *optarg;
  extern int optind, optopt;
  
  if(argc < 2){
    system(PS);
  }else if(argc == 2){
    const char* param = argv[1];
    if(strlen(param) > 0){
      runCommand(param);
    }else{
      system(PS);
    }
  }else if(argc > 2){
    while((c = getopt(argc, argv, ":k:")) != -1){
      switch(c){
        case 'k':{
          kflg++;
          targetPid = atoi(optarg);
        }
        break;
        case ':':{
          fprintf(stderr, "Option -%c requires an operand\n", optopt);
          errflg++;
        }
        break;
        case '?': {
          fprintf(stderr, "Incorrect option %c\n", optopt);
        }
        break;
      }
    }
    if(kflg > 0){
      killProcess(targetPid);
    }
  }

  return EXIT_SUCCESS;
}

void runCommand(const char* param){
  char* command;
  const char* ps = "ps -xeo pid,comm ";
  const char* grep = "grep ";
  const char* pipe = "|";
  command = calloc(strlen(PS) + strlen(GREP) + strlen(param) + strlen(PIPE), sizeof(char));
  strncat(command, ps, strlen(ps));
  strncat(command, pipe, strlen(pipe));
  strncat(command, grep, strlen(grep));
  strncat(command, param, strlen(param));
  int result = system(command);

}

void killProcess(int pid){
  kill(pid, SIGKILL);
}

char* extractSwitches(const char* params){

}
